# @tozd/vue-router-referer

This NPM package provides a Vue plugin which exposes the [route object](https://router.vuejs.org/api/#the-route-object)
of the previous route for [Vue Router](https://router.vuejs.org),
under `this.$router.referer`.

## Installation

This is a NPM package. You can install it using NPM:

```bash
$ npm install @tozd/vue-router-referer
```

It requires `vue` and `vue-router` peer dependencies:

```bash
$ npm install vue vue-router
```

## Usage

First, you have to register the package as a Vue plugin:

```js
import Vue from 'vue';
import VueRouterReferer from '@tozd/vue-router-referer';

Vue.use(VueRouterReferer);
```

## See Also

For more context, see [this issue](https://github.com/vuejs/vue-router/issues/883).
